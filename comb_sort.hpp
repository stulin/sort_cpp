#ifndef COMB_SORT_HPP
#define COMB_SORT_HPP
#include <algorithm>

template <typename T>
void comb_sort(T* l, T* r) {
	size_t size = r - l;
	if (size <= 1) return;
	double k = 1.2473309;
	size_t step = size - 1;
	while (step > 1) {
		for (int* i = l; i + step < r; i++) {
			if (*i > *(i + step))
				std::swap(*i, *(i + step));
		}
		step /= k;
	}
	bool b = true;
	while (b) {
		b = false;
		for (T* i = l; i + 1 < r; i++) {
			if (*i > *(i + 1)) {
				std::swap(*i, *(i + 1));
				b = true;
			}
		}
	}
}

template <typename T>
void comb_sort(T arr[], size_t size) {
    auto l = arr;
    auto r = arr + size;
    comb_sort(l, r);
}



#endif //COMB_SORT_HPP
