#ifndef INSERTION_SORT_HPP
#define INSERTION_SORT_HPP
#include <algorithm>
//O(n²)
template <typename T>
void insertion_sort(T* l, T* r) {
	for (int *i = l + 1; i < r; i++) {
		int* j = i;
		while (j > l && *(j - 1) > *j) {
			std::swap(*(j - 1), *j);
			j--;
		}
	}
}

template <typename T>
void insertion_sort(T arr[], size_t size) {
    auto l = arr;
    auto r = arr + size;
    insertion_sort(l, r);
}



#endif //INSERTION_SORT_HPP
