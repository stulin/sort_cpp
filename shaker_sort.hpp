#ifndef SHAKER_SORT_HPP
#define SHAKER_SORT_HPP
#include <algorithm>
//O(n²)
template <typename T>
void shaker_sort(T* l, T* r) {
	size_t size = r - l;
	if (size <= 1) return;
	bool b = true;
	T* beg = l - 1;
	T* end = r - 1;
	while (b) {
		b = false;
		beg++;
		for (T* i = beg; i < end; i++) {
			if (*i > *(i + 1)) {
				std::swap(*i, *(i + 1));
				b = true;
			}
		}
		if (!b) break;
		end--;
		for (int* i = end; i > beg; i--) {
			if (*i < *(i - 1)) {
				std::swap(*i, *(i - 1));
				b = true;
			}
		}
	}
}

template <typename T>
void shaker_sort(T arr[], size_t size) {
    auto l = arr;
    auto r = arr + size;
    shaker_sort(l, r);
}

#ifdef ALTERNATIVE_SOLUTION
#include <cstddef>
#include <utility>
template<typename T>
void shaker_sort(T array[], std::size_t size) {
    for (size_t left_idx = 0, right_idx = size - 1; left_idx < right_idx;) {
        for (size_t idx = left_idx; idx < right_idx; idx++) {
            if (array[idx + 1] < array[idx]) {
                std::swap(array[idx], array[idx + 1]);
            }
        }
        right_idx--;

        for (size_t idx = right_idx; idx > left_idx; idx--) {
            if (array[idx - 1] >  array[idx]) {
                std::swap(array[idx - 1], array[idx]);
            }
        }
        left_idx++;
    }
}
#endif //ALTERNATIVE_SOLUTION

#endif //SHAKER_SORT_HPP
