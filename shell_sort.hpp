#ifndef SHELL_SORT_HPP
#define SHELL_SORT_HPP

template <typename T>
void shell_sort(T arr[], size_t size) {
	T t;
	size_t i, j, k;
	for (k = size/2; k > 0; k /= 2) {
        for (i = k; i < size; i++) {
            t = arr[i];
            for(j = i; j >= k; j -= k) {
                if (t < arr[j-k]) arr[j] = arr[j-k];
                else break;
            }
            arr[j] = t;
        }
    }
}

#endif //SHELL_SORT_HPP
