#ifndef BUBBLE_SORT_HPP
#define BUBBLE_SORT_HPP
#include <algorithm>

template <typename T>
void bubble_sort(T arr[], size_t size) {
    for (size_t j = 0; j < size-1; ++j)
        for (size_t i = 0; i < size-j-1; ++i)
            if (arr[i] > arr[i+1])
                std::swap(arr[i], arr[i+1]);
}

#endif //BUBBLE_SORT_HPP
