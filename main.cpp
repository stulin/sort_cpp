#include <iostream>
#include "CSV.hpp"

#include "bubble_sort.hpp"
#include "quick_sort.hpp"
#include "shaker_sort.hpp"
#include "comb_sort.hpp"
#include "insertion_sort.hpp"
#include "shell_sort.hpp"

using namespace std;


int lineNumberInFile(const char* filename) {
    FILE* fp;
    size_t cnt = 0u;
    if ((fp = fopen(filename, "r")) == NULL)
        return 0u;
    while (!feof(fp)) {
        fscanf(fp, "%*[^\n]%*c");
        cnt++;
    }
    fclose(fp);
    return cnt - 1;
}


bool isSort(int* arr, size_t size) {
    for(size_t i = 0; i < size-1; i++) {
        if (arr[i+1] < arr[i]) {
            return false;
        }
    }
    return true;
}

void runSort(void(*sortfn)(int*, size_t),
             const char* result_file,
             size_t posy, const char* compiler,
             size_t posx, const char* sortname) {
    cout << sortname << " sort...\n";
    int size = lineNumberInFile("../sequence.txt");
    int* arr = new int[size];
    std::ifstream f;
    time_t start, finish;
    CSV objCSV; objCSV.open("../stat.csv");
    
    f.open("../sequence.txt");
    if (!f) { cerr << "File access problem.\n"; exit(1); }
    {int i = 0;
    while(!f.eof() && i < size) {
        f >> arr[i];
        i++;
    }}
    f.close();

    time(&start);
        (*sortfn)(arr, size);
    time(&finish);
    
    if (!isSort(arr, size)) std::cerr<<"Массив не отсортирован\n";
    if(0) { //Запись результатов в файл отключена
    std::ofstream out_file(result_file);
    for(int i = 0; i < size; ++i) out_file << arr[i] << std::endl;
    out_file.close();
    }

    objCSV(0, posy) = compiler;
    objCSV(posx, 0) = sortname;
    stringstream ss; ss << finish - start;
    cout<<"Time: "<<finish-start<<" seconds."<<endl;
    objCSV(posx, posy) = ss.str();
    objCSV.save("../stat.csv");
    
    delete[] arr;
}

int main() {
    size_t variant;
    const char* compiler =
#if defined(__GNUC__)
    "g++";
#elif defined(__clang__)
    "clang++";
#else
    "unknown";
#endif
    runSort(bubble_sort,    "result/bubble.txt",    1, compiler, 1, "bubble"   );
    runSort(quick_sort,     "result/quick.txt",     1, compiler, 2, "quick"    );
    runSort(shaker_sort,    "result/shaker.txt",    1, compiler, 3, "shaker"   );
    runSort(comb_sort,      "result/comb.txt",      1, compiler, 4, "comb"     );
    runSort(insertion_sort, "result/insertion.txt", 1, compiler, 5, "insertion");
    runSort(shell_sort,     "result/shell.txt",     1, compiler, 6, "shell"    );
}
