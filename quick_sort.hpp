#ifndef QUICK_SORT_HPP
#define QUICK_SORT_HPP
#include <algorithm>

template <typename T>
void quick_sort(T arr[], size_t first, size_t last) {
    size_t left_index = first, right_index = last;
    T pivot_value = arr[first + (last - first) / 2];
    do {
        while (arr[left_index] < pivot_value) left_index++;
        while (arr[right_index] > pivot_value) right_index--;
        if (left_index <= right_index) {
            if (arr[left_index] > arr[right_index]) {
                std::swap(arr[left_index], arr[right_index]);
            }
            left_index++;
            right_index--;
        }
    } while (left_index <= right_index);
    if (left_index < last)
        quick_sort(arr, left_index, last);
    if (right_index > first)
        quick_sort(arr, first, right_index);
}

template <typename T>
void quick_sort(T arr[], size_t size) {
    quick_sort(arr, 0, size-1);
}

#endif //QUICK_SORT_HPP
